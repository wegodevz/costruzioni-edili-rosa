<?php return array(
    'root' => array(
        'name' => 'gbiorczyk/webp-converter-for-media',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'a71f2a1443f61556385a6a5f5188c9cc75bd0687',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'gbiorczyk/webp-converter-for-media' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'a71f2a1443f61556385a6a5f5188c9cc75bd0687',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'matt-plugins/deactivation-modal' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => 'dc52aa8c8793d1c11eb973c0f77b63f8abc2d9bf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../matt-plugins/deactivation-modal',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
