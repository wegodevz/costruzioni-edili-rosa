<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via web
 * puoi copiare questo file in «wp-config.php» e riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Chiavi Segrete
 * * Prefisso Tabella
 * * ABSPATH
 *
 * * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', 'edilirosa_db' );

/** Nome utente del database MySQL */
define( 'DB_USER', 'edilirosa_user' );

/** Password del database MySQL */
define( 'DB_PASSWORD', 'xm0Rt93~jNsr162$' );

/** Hostname MySQL  */
define( 'DB_HOST', '212.25.183.22' );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_m]13}cRLLxrad?p}T0*K[8ZsR*oNkF1-<NLe!%&#9`=;6bTKLT[oyRij}Uqh@Yy' );
define( 'SECURE_AUTH_KEY',  '[6J@jy/<kZRxM2FO~RJR`Ir8M`m [bl5FE8DSPIz.V_6GOfmk<WO0 2)j`5TUB~c' );
define( 'LOGGED_IN_KEY',    '~4,BYHQlvm]Oh|vFPtCH33syjY_jB%h-y490W.e;S?sO}Ktn9fPs]03Ll^/~~jhP' );
define( 'NONCE_KEY',        '=I4]VP yMU{.;o>eXS)IVeNz;9XoRM~-$ #3*ROu5}ADO+Vauy^`R_5KmK^XV]dk' );
define( 'AUTH_SALT',        '63]^cLLH-6r1>hE8)I4[8l<K$Yx20:WvV$y+kjAd{VClYx`0abw3 a5&zBb)]`GY' );
define( 'SECURE_AUTH_SALT', '%#?1QL4}4+X;iPxna[Lt:Fi,-NzV1svrfoQwX=kS687?x~xU|SQpt#J/M?!OF+ZA' );
define( 'LOGGED_IN_SALT',   '+?_BBky rEmg{Pg[.mq&n5YWZG_}ipIa-x8X[akC`y&2o;_=)Si$+TU:gVl9t-~E' );
define( 'NONCE_SALT',       'Z}eyekW}H@ sio!WkoeLrbEtebW<7la,nt~<?$|U[YV=g9t`3qb)tU)Dy%+pd .F' );

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'er_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi durante lo sviluppo
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 *
 * Per informazioni sulle altre costanti che possono essere utilizzate per il debug,
 * leggi la documentazione
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
